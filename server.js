//console.log('Welcome Areej');
//console.log('Hello There!');
//console.log('Take care');
//nodemon - hot reload or live server;
//console.log('Finally ariel happen to  me');
//console.log('hello');

const express = require('express'); //ung express after const, kahit ano yan pero ung sa loob ng function dapat ung name ng folder ng node modules
const cors = require('cors'); //eto inimport na natin si cors
const viewersRoutes = require('./routes/Viewer'); //dineclare natin dito na nirerequire niya ung nasa loob ng routes folder kasi gagmitin nating tong path na to.. eto ung inaaccess dito is ung nasa folder na routes
//ginagwa lang kapital ung Viewers para madistingues
const mongoose = require('mongoose');
const api = express();

//
//
//Middleware - kasi ung binabato ng client natin ay JSON format, eto ung magbabasa ng JSON format natin
api.use(express.json());
api.use(express.urlencoded({ extended: true })); //built in function ng app 

//eto na ung ggagamitin natin si cors para bigyan ng permission lahat ng icomming request
//para e permit lahat ng incoming request si cors. Is an HTTP mechanism that allows a server to indicate origins 
api.use(cors());

//connect to mongodb atlas via mongoose
mongoose.connect('mongodb+srv://admin:panutsaILOVEYOU11@wdc028-course-booking.lksmc.mongodb.net/dbBooking_app?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});

//set notification for connection success or failure
let db = mongoose.connection;
//if connection error
db.on('error', (console.error.bind(console, 'connection error: ')));


//once connection, show notif
db.once('open', () => console.log("Were connected to our database"));
/*mongoose {
	connection {
		status: 'error'
		once: () => {}
		on: () => {}
	}
}*/
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: 'pending'
    }

})

const userSchema = new mongoose.Schema({
    username: String,
    email: String,
    password: String,
    task: [taskSchema]

});
const User = mongoose.model('User', userSchema);




const Task = mongoose.model('Task', taskSchema);

// mongoose {
// 	save: () => {}

// }




//register a new user
// api.post('/user', (req, res) => {
//     //check if username or email are duplicate prior to registration
//     User.find({ $or: [{ username: req.body.username }, { email: req.body.email }] }, (findErr, duplicates) => {
//         if (findErr) return console.error(findErr);
//         //if duplicates found
//         if (duplicates.length > 0) {
//             return res.status(403).json({
//                 message: "Duplicates found, kindly choose different username and/or email."
//             })

//         } else {
//             //instantiate a new user object with properties derived from the request body
//             let newUser = new User({
//                 username: req.body.username,
//                 email: req.body.email,
//                 password: req.body.password,
//                 //tasks is initially an empty array
//                 tasks: []
//             })
//             //save
//             newUser.save((saveErr, newUser) => {
//                 //if an error was ecncounter while saving the document - notification in the console
//                 if (saveErr) return console.error(saveErr);
//                 return res.status(201).json({
//                     message: `User ${newUser.username} successfull registered,`,
//                     data: {
//                         username: newUser.username,
//                         email: newUser.email,
//                         link_to_self: `/user/${newUser._id}`
//                     }
//                 });
//             })
//         }
//     })
// });
// 
//display user
api.get('/user/:id',(req, res) => {
	User.findById(req.params.id,(err, user) => {
		if(err) return console.error(err);
		return res.status(200).json({
			message: "User retrieved successfully. ",
			data: {
				username: user.username, 
				email: user.email,
				tasks: `/user/${user._id}/tasks`
			}
		})
	})
})


/***********/

api.post("/user", (req, res) => {
    User.find({
            $or: [{
                    username: req.body.username
                },
                {
                    email: req.body.email
                }
            ]
        },
        (findErr, duplicates) => {
            if (findErr) 
                return console.error(findErr);
            //if duplicates found
            if (duplicates.length > 0) {
                return res.status(403).json({
                    message: "Duplicates found, kindly choose different username and/or email."
                })
            } else {
                let newUser = new User({
                    username: req.body.username,
                    email: req.body.email,
                    password: req.body.email,
                    password: req.body.password,
                    //task initially empty array
                    task: []

                })
                //save
                newUser.save((saveErr,newUser) => {
                    if (saveErr) {
                        return console.log(saveErr);
                    }
                    return
                    res.status(201).json({
                        message: `User ${newUser.username} successfully registered,`,
                        data: {
                            username: newUser.username,
                            email: newUser.email,
                            link_to_self: `/user/${newUser._id}`
                        }
                    })
                })
            }
        }
    )
    //if may naencounter na error, mag display ng notif
})

/********************/


//routes
api.use('/', viewersRoutes); //tinatawag natin si viewers eto ung entry point
//dito pwedeng slash lang.. api.use('/viewers',viewersRoutes); pwedeng walang viewers
//path yan, tayo nagdedefine niyan
//ang path kasi na yan, ang inaaccess niya yung viewersRoutes

//create port - communication
//usually ang default ay 3000 pero kahit naman basta dineclare mo siya
const port = 3000;

api.listen(port, () => { console.log(`Listening on port ${port}.`) }) //eto ung parang command para icheck kung okay at running ung port

// port entrypoint get method